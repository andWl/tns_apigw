'user strict'
var path = require('path');
var fs = require('fs');
var util = require('util');
var hfc = require('fabric-client');
//var hfc = require('../fabric-sdk-node/fabric-client/');
var app = require('../app.js');
var helper = require('./helper.js');
var logger = helper.getLogger('invoke-chaincode');
logger.setLevel('ERROR');
// var AsyncPolling = require('async-polling');

// global.PromiseItems = [];

// let IsLocked = false;


// //UNDER CONSTRUCTION DO NOT TOUCH...


// AsyncPolling(async function (end) {
	
// 	if(IsLocked)
// 		return;
// 	IsLocked = true;
	
// 	while(global.PromiseItems.length > 0) {
// 		//logger.error("Count : " + global.PromiseItems.length);	
// 		let promise = global.PromiseItems.pop();
		
// 		await promise;
// 		logger.error(promise);
		
// 		promise = null;
// 	}
	
// 	//let result = await Promise.all(global.PromiseItems);
// 	//global.PromiseItems = [];
	
// 	IsLocked = false;
// 	end();
	
// }, 100).run();

function getReturnMessage(result, field) {
	var response = {
		success: result,
		message: field
	};
	return response;
}


async function getClient(org_name, username) {
	var client = await helper.getClientForOrg(org_name, username);
	return client;
}

var invokeChaincode = async function(location, chaincodeName, fcn, args, username, org_name) {
	console.time('invokeChaincode');
	
	logger.debug(util.format('\n============ invoke transaction on channel %s ============\n', location.channelName));
		
	var error_message = null;
	var tx_id_string = null;
	var block_num_string = null;
	var orderer_request = null;
	try {
		// first setup the client for this org		
		//var client = await helper.getClientForOrg(org_name, username);
		logger.debug('Successfully got the fabric client for the organization "%s"', org_name);
		var channel = location.channel;
		if(!channel) {
			let message = util.format('Channel %s was not defined in the connection profile', location.channelName);
			logger.error(message);
			//throw new Error(message);
			return getReturnMessage(false, message);
		}
	
		var tx_id = global.client.newTransactionID();
		//logger.info('tx_id ==> ', tx_id);
				
		
		// will need the transaction ID string for the event registration later
		tx_id_string = tx_id.getTransactionID();
		
		
		// send proposal to endorser
		var request = {
			targets: location.peer,
			chaincodeId: chaincodeName,
			fcn: fcn,
			args: args,
			chainId: location.channelName,
			txId: tx_id
		};
		

		//let results = await channel.sendTransactionProposal(request);
		let results = await channel.sendTransactionProposalOB(request);

		// the returned object has both the endorsement results
		// and the actual proposal, the proposal will be needed
		// later when we send a transaction to the orderer
		var proposalResponses = results[0];
		var proposal = results[1];

		// lets have a look at the responses to see if they are
		// all good, if good they will also include signatures
		// required to be committed
		var all_good = true;
		for (var i in proposalResponses) {
			let one_good = false;
			if (proposalResponses && proposalResponses[i].response &&
				proposalResponses[i].response.status === 200) {
				one_good = true;
				logger.info('invoke chaincode proposal was good');
			} else {
				logger.error('invoke chaincode proposal was bad');
			}
			all_good = all_good & one_good;
		}
				

		if (all_good) {
			// logger.info(util.format(
			// 	'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s',
			// 	proposalResponses[0].response.status, proposalResponses[0].response.message,
			// 	proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature));

			/*
			logger.info(util.format(
				'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
				proposalResponses[0].response.status, proposalResponses[0].response.message));
			*/

			// wait for the channel-based event hub to tell us
			// that the commit was good or bad on each peer in our organization
			var promises = [];
			
			/*
			let event_hubs = channel.getChannelEventHubsForOrg();
			event_hubs.forEach((eh) => {
				logger.debug('invokeEventPromise - setting up event');
				let invokeEventPromise = new Promise((resolve, reject) => {
					let event_timeout = setTimeout(() => {
						let message = 'REQUEST_TIMEOUT:' + eh.getPeerAddr();
						logger.error(message);
						eh.disconnect();
					}, 3000);
					eh.registerTxEvent(tx_id_string, (tx, code, block_num) => {
						logger.info('The chaincode invoke chaincode transaction has been committed on peer %s',eh.getPeerAddr());
						logger.info('Transaction %s has status of %s in BLOCK %s', tx, code, block_num);
						block_num_string = block_num;
						clearTimeout(event_timeout);

						if (code !== 'VALID') {
							let message = util.format('The invoke chaincode transaction was invalid, code:%s',code);
							logger.error(message);
							reject(new Error(message));
						} else {
							let message = 'The invoke chaincode transaction was valid.';
							logger.info(message);
							resolve(message);
						}
					}, (err) => {
						clearTimeout(event_timeout);
						logger.error(err);
						reject(err);
					},
						// the default for 'unregister' is true for transaction listeners
						// so no real need to set here, however for 'disconnect'
						// the default is false as most event hubs are long running
						// in this use case we are using it only once
						{unregister: true, disconnect: false}
					);
					eh.connect();
				});
				promises.push(invokeEventPromise);
			});
			*/

			//console.time('send transaction to orderer');
			orderer_request = {
				txId: tx_id,
				proposalResponses: proposalResponses,
				proposal: proposal
			};
			
			//var sendPromise = channel.sendTransaction(orderer_request);
			//var sendPromise = channel.sendTransactionOB(orderer_request);
			
			//await channel.sendTransactionOB(orderer_request);
			// let sendPromise = channel.sendTransactionOB(orderer_request);
			// global.PromiseItems.push(sendPromise);

			/*
			channel.sendTransactionOB(orderer_request)
				.then(function (response) {
					logger.info(response);
				})
				.catch(function (err) {
					logger.error(err);
				});
			*/
			//console.timeEnd('send transaction to orderer');
			
			//console.timeEnd('invokeChaincode');
			
			// put the send to the orderer last so that the events get registered and
			// are ready for the orderering and committing
			
			//promises.push(sendPromise);
			//let results = await Promise.all(promises);
			
			//global.gc();


			/*
			logger.debug(util.format('------->>> R E S P O N S E : %j', results));
			let response = results.pop(); //  orderer results are last in the results
			if (response.status === 'SUCCESS') {
				logger.info('Successfully sent transaction to the orderer.');
			} else {
				error_message = util.format('Failed to order the transaction. Error code: %s',response.status);
				logger.debug(error_message);
			}
			
			
			// now see what each of the event hubs reported
			for(let i in results) {
				let event_hub_result = results[i];
				let event_hub = event_hubs[i];
				logger.debug('Event results for event hub :%s',event_hub.getPeerAddr());
				if(typeof event_hub_result === 'string') {
					logger.debug(event_hub_result);
				} else {
					if(!error_message) error_message = event_hub_result.toString();
					logger.debug(event_hub_result.toString());
				}
			}
			*/
		} else {
			error_message = util.format('Failed to send Proposal and receive all good ProposalResponse');
			logger.debug(error_message);
		}
	} catch (error) {
		logger.error('Failed to invoke due to error: ' + error.stack ? error.stack : error);
		error_message = error.toString();
	}

	if (!error_message) {
		let message = util.format(
			'Successfully invoked the chaincode %s to the channel \'%s\' for transaction ID: %s',
			org_name, location.channelName, tx_id_string);
		logger.info(message);
		//return getReturnMessage(true, block_num_string);
		return getReturnMessage(true, orderer_request);
	} else {
		let message = util.format('Failed to invoke chaincode. cause:%s', error_message);
		logger.error(message);
		//throw new Error(message);
		return getReturnMessage(false, message);
	}

};

var sendTransaction = function(location, orderer_request) {
	console.time('sendTransaction');
	let channel = location.channel;
	 channel.sendTransaction(orderer_request);
}

exports.sendTransaction = sendTransaction;
exports.invokeChaincode = invokeChaincode;
exports.getClient = getClient;
