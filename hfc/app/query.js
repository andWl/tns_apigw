var path = require('path');
var fs = require('fs');
var util = require('util');
var hfc = require('fabric-client');
var helper = require('./helper.js');
var logger = helper.getLogger('Query');
logger.setLevel('debug');

const Shared = require('mmap-object');

function getReturnMessage(result, field) {
	var response = {
		success: result,
		message: field
	};
	return response;
}

var queryChaincode = async function(location, chaincodeName, args, fcn, username, org_name) {
	logger.debug('\n\n============ Query Chaincode ' + chaincodeName + ' ============\n');

	var error_message = null;
	var eventhubs_in_use = [];
	var peer_name = location.peer;

	try {
		//var client = await helper.getClientForOrg(org_name, username);
		logger.debug('Successfully got the fabric client for the organization "%s"', org_name);
		var channel = location.channel;
		if(!channel) {
			let message = util.format('Channel %s was not defined in the connection profile', channelName);
			logger.error(message);
			var response = {
				success: false,
				message: message
			};
			return response;
			// throw new Error(message);
		}

		// connected orderer, peer info
		try {
                        const read_only_shared_object = new Shared.Open(path.join(__dirname, './peer-orderer-default'));
                        peer_name = read_only_shared_object.checked_peer;
                        read_only_shared_object.close();

                        logger.debug('shared peer : ', peer_name);

                } catch (err) {
                        logger.debug(err);
                }


		// send query
		var request = {
			targets : [peer_name], //queryByChaincode allows for multiple targets
			chaincodeId: chaincodeName,
			fcn: fcn,
			args: args
		};

		let response_payloads = await channel.queryByChaincode(request)
		if (response_payloads) {
			for (let i = 0; i < response_payloads.length; i++) {
				var res_payload = response_payloads[i].toString('utf8')
				if(res_payload.search("Error:") != -1) {
					logger.error('Failed to query due to error: ' + res_payload);
					return getReturnMessage(false, res_payload);
				}
				logger.info(args[0]+' =' + res_payload);
			}
			return getReturnMessage(true,response_payloads[0].toString('utf8'));
		} else {
			logger.error('response_payloads is null');
			return getReturnMessage(false, 'response_payloads is null');
		}

	} catch(error) {
		logger.error('Failed to query due to error: ' + error.stack ? error.stack : error);
		return getReturnMessage(false, error.toString());

	}

};

module.exports.queryChaincode = queryChaincode;
