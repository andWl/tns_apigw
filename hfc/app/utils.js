'use strict';
var request = require('request');
var axios = require('axios');
var helper = require('./helper.js');
var logger = helper.getLogger('utils');
var md5 = require('md5');
logger.setLevel('INFO');

const baseUrl = 'http://localhost:4000';
var missingMsg = " field is missing or Invalid in the request'";

var getErrorMessage = function(field) {
  var response = {
    result: "fail",
    message: field
  };
  return response;
}

//var temp = 0;
//header value
var getPeers = function (data) {
  var hashToInteger = parseInt(md5(data[0]).substring(0,2),16);
  
  //var idx = hashToInteger % global.peerArr.length;
  var idx = 0;
 
  var peer = global.peerArr[idx];

  /*
  if(peer.org === 'Org1') {
    peer.client = global.clientOrg1;
  } else if(peer.org === 'Org2') {
    peer.client = global.clientOrg2;
  }
  */

  //console.log(peer);

  //return global.peerArr[idx];
  return peer;
  
  // for Test
  /*
  var idxTemp = temp % 2;
  temp++;
  return global.peerArr[3];
  */
}

exports.getErrorMessage = getErrorMessage;
exports.getPeers = getPeers;
