'use strict';
var model = require('./model.js');
var helper = require('./helper.js');
var logger = helper.getLogger('peer-status');
logger.setLevel('INFO');

var KeyData = model.KeyData;
var peerStatusArr = model.peerStatusArr;

var changeStatus = function(channelName, status) {
  var index = peerStatusArr.findIndex(x => x.channel=== channelName);
  
  if (index <= -1) {
    throw new Error(`Channel with name ${channelName} not Exist`);
  } else {
    if(status === 0) {
      peerStatusArr[index].currentRunTx = status;
    } else {
      peerStatusArr[index].currentRunTx += status;
    }
    return true;
  }
};

var findPeer = function(_key) {
  logger.debug(">> find Peer :", _key[0]);
  var keyData = KeyData.findOne({key:_key[0]});
  return keyData;
}

var getIdlePeer = async function(_key) {
  logger.debug(">> get Idle Peer :", _key[0]);
  var savedPeer = await findPeer(_key);

  var currentRunTxArr = peerStatusArr.map(a => a.currentRunTx);
  var minRunTx = Math.min(...currentRunTxArr);
  var arrIdleChannel = [...peerStatusArr].filter(x => x.currentRunTx === minRunTx);

  var minBlockArr = arrIdleChannel.map(a => a.blockNum);
  var minBlockNum = Math.min(...minBlockArr);
  var idleChannel = ([...arrIdleChannel].find(x => x.blockNum == minBlockNum));

  if(savedPeer == undefined) {
    var result = {
      key : _key[0],
      peer : idleChannel.peer,
      channel : idleChannel.channel
    }
    await saveData(result);
	
	logger.info(result.peer);
    return result;
  } else {
	  logger.info(savedPeer);
    return savedPeer;
  }
}


var saveData = function(data) {
  var newKeyData = new KeyData({key : data.key, channel : data.channel, peer:data.peer});
  newKeyData.save().then(function(data) {
    return data;
  }).catch(function (err) {
    throw new Error(err);
  });
};

var saveBlockNum = function(channel, blockNum) {
  var index = peerStatusArr.findIndex(x => x.channel=== channel);

  if (index <= -1) {
    throw new Error(`Channel with name ${channel} not Exist`);
  } else {
    peerStatusArr[index].blockNum = blockNum;
    return true;
  }
}


module.exports.changeStatus = changeStatus;
module.exports.getIdlePeer = getIdlePeer;
module.exports.findPeer = findPeer;
module.exports.saveData = saveData;
module.exports.saveBlockNum = saveBlockNum;