'use strict';
var log4js = require('log4js');
var logger = log4js.getLogger('Helper');
logger.setLevel('ERROR');

var path = require('path');
var util = require('util');
var copService = require('fabric-ca-client');

var hfc = require('fabric-client');
//var hfc = require('../fabric-sdk-node/fabric-client/');
hfc.setLogger(logger);
var ORGS = hfc.getConfigSetting('network-config');

var clients = {};
var channels = {};
var caClients = {};

async function getClientForOrg (userorg, username) {
	logger.debug('getClientForOrg - ****** START %s %s', userorg, username)
	let config = '-connection-profile-path';
	
	//console.log('hfc.getConfigSetting(network+config)', hfc.getConfigSetting('network'+config));

	let client = hfc.loadFromConfig(hfc.getConfigSetting('network'+config));
	

	client.loadFromConfig(hfc.getConfigSetting(userorg+config));

	await client.initCredentialStores();

	if(username) {
    let user = await client.getUserContext(username, true);
    if(!user) {
				let response = {
					success: false,
					message: util.format('User was not found :', username)
				};
				return response;
        //throw new Error(util.format('User was not found :', username));
    } else {
        logger.debug('User %s was found to be registered and enrolled', username);
    }
  }
	logger.debug('getClientForOrg - ****** END %s %s \n\n', userorg, username)

	return client;
}


var getRegisteredUser = async function(username, userOrg, isJson) {
	logger.debug('getRegisteredUser - ***** START %s %s %s', username, userOrg, isJson);

	try {
		var client = await getClientForOrg(userOrg);
    logger.debug('Successfully initialized the credential stores');

		var user = await client.getUserContext(username, true);
		if (user && user.isEnrolled()) {
        logger.info('Successfully loaded member from persistence');
    } else {
      // user was not enrolled, so we will need an admin user object to register
      logger.info('User %s was not enrolled, so we will need an admin user object to register',username);
			var admins = hfc.getConfigSetting('admins');

      let adminUserObj = await client.setUserContext({username: admins[0].username, password: admins[0].secret});

      let caClient = client.getCertificateAuthority();
      let secret = await caClient.register({
        enrollmentID: username,
        affiliation: userOrg.toLowerCase() + '.department1'
      }, adminUserObj);
      logger.debug('Successfully got the secret for user %s',username);
      user = await client.setUserContext({username:username, password:secret});
      logger.debug('Successfully enrolled username %s  and setUserContext on the client object', username);
		}

		if(user && user.isEnrolled) {
      if (isJson && isJson === true) {
        var response = {
          success: true,
          secret: user._enrollmentSecret,
          message: username + ' enrolled Successfully',
        };
        return response;
      }
    } else {
      throw new Error('User was not enrolled ');
    }

	} catch(error) {
		logger.error('error getRegisteredUser');
		logger.error('Failed to get registered user: %s with error: %s', username, error.toString());
		return 'failed '+error.toString();
	}

};

var getLogger = function(moduleName) {
        var logger = log4js.getLogger(moduleName);
        logger.setLevel('DEBUG');
        return logger;
};

module.exports.getClientForOrg = getClientForOrg;
module.exports.getRegisteredUser = getRegisteredUser;
module.exports.getLogger = getLogger;
