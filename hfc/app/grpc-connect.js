var net = require('net');


var connect = function(servername, port) {

  return new Promise(function(resolve, reject) {
    let result = false;
    var client = net.connect(
      {host:servername,port:port},
      function(data) {
        client.end();
        resolve('connect');
      }
    );

    client.on('error', function (err) {
        reject(err);
    });
  })

}

exports.connect = connect
