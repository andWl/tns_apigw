'use strict';

var log4js = require ('log4js');
var logger = log4js.getLogger('App');
logger.setLevel('ERROR');

var express = require('express');
var session = require('express-session');

var path = require('path');
var expressValidator = require('express-validator');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
var util = require('util');
var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');
var bearerToken = require('express-bearer-token');
var cors = require('cors');
var compression = require('compression');

var users = require('./routes/users');
var channels = require('./routes/channels');
var gateway = require('./routes/gateway');
var invokeTransaction = require('./app/invoke-transaction');



var orgName = 'Org1';
var userName = 'User1.org1';

var client = {};

invokeTransaction.getClient(orgName, userName)
        .then(function(tmpClient) {
                client = tmpClient;
                //console.log('clientOrg1', client);
                global.client = client;

                for(var i = 0; i < peerArr.length; i++) {
                        peerArr[i].channel = client.getChannel(peerArr[i].channelName);

                }

                //logger.debug(peerArr);
        });



var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(compression());



//Validator
app.use(expressValidator({
	errorFormatter : function(param, msg, value) {
		 return msg;
	}
}));

// token 없이 사용가능하도록 임시 주석처리
/*
 app.set('secret', 'thisismysecret');
 app.use(expressJWT({
 	secret: 'thisismysecret'
 }).unless({
 	path: ['/api/users']
 }));

*/
app.use(bearerToken());


app.use(function(req, res, next) {
	logger.debug(' ------>>>>>> new request for %s',req.originalUrl);

        req.connection.setNoDelay(true);

	if (req.originalUrl.indexOf('/api/users') >= 0) {
		return next();
	}

  // token 없이 사용가능하도록 임시 주석처리
 /*
	 var token = req.token;
	 jwt.verify(token, app.get('secret'), function(err, decoded) {
	 	if (err) {
	 		res.send({
	 			success: false,
	 			message: 'Failed to authenticate token. Make sure to include the ' +
	 				'token returned from /users call in the authorization header ' +
	 				' as a Bearer token'
	 		});
	 		return;
	 	} else {
	 		// add the decoded user name and org name to the request object
	 		// for the downstream code to use
	 		req.username = decoded.username;
	 		req.orgname = decoded.orgName;
	 		logger.debug(util.format('Decoded from JWT token: username - %s, orgname - %s', decoded.username, decoded.orgName));
	 		return next();
	 	}
	 });
*/

	req.username = userName;
	req.orgname = orgName;
	return next();
});


app.use('/api/users', users);
app.use('/api/channels', channels);
app.use('/', gateway);
console.log(require('v8').getHeapStatistics().heap_size_limit);

//var heapdump = require('heapdump');

console.log('execArgv : ' + process.execArgv);

module.exports = app;
