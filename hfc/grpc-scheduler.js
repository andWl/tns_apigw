'use strict';

var path = require('path');
var log4js = require ('log4js');
var logger = log4js.getLogger('App');
logger.setLevel('debug');

const Shared = require('mmap-object');
const shared_object = new Shared.Create('./app/peer-orderer-default');

var yaml = require('js-yaml');
var fs   = require('fs');
var cron = require('node-cron');
var grpcConnect = require('./app/grpc-connect.js');

var nwCfg = new Object();
nwCfg.channels = new Array();
nwCfg.peers = new Array();
nwCfg.orderers = new Array();


try {
	var config = yaml.safeLoad(fs.readFileSync(path.join(__dirname, './config/network-config.yaml'), 'utf8'));
} catch (e) {
	throw new Error(e);
}

var channels = Object.keys(config.channels);
for(var idx in channels) {
	nwCfg.channels.push(channels[idx])
};

var peers = Object.keys(config.peers);
var orderers = Object.keys(config.orderers);


getPeerInfo();
getOrdererInfo();

console.log('network config : \n', JSON.stringify(nwCfg, null, 2));


var shared_peer = peers[0];
var shared_orderer = orderers[0];
shared_object['checked_peer'] = shared_peer;
shared_object['checked_orderer'] = shared_orderer;


cron.schedule('*/30 * * * * *', async function() {

        shared_orderer = await grpcConnectCheck('orderers', shared_orderer, 'checked_orderer');
        shared_peer = await grpcConnectCheck('peers', shared_peer, 'checked_peer');

	// grpcConnectCheck('orderers', shared_orderer, 'checked_orderer')
        // .then(function(result) {
               // shared_orderer = result;
        //     // console.log('result : ', result);
        //     // console.log('shared_orderer : ', shared_orderer);
        // });
	//
        // grpcConnectCheck('peers', shared_peer, 'checked_peer')
        // .then(function(result) {
        //     // shared_peer = result;
        //     // console.log('result : ', result);
        //     // console.log('shared_peer : ', shared_peer);
        // });

});


function splitURL(url) {
	var tmp = url.split('//');
	tmp = tmp[1].split(':');

	var host = tmp[0];
	var port = tmp[1];	

        return [host, port];

};


function getPeerInfo() {
	for(var idx in peers) {
		var segURL = splitURL(config.peers[peers[idx]].url);
                //console.log(segURL[0], segURL[1]);
                nwCfg.peers.push({ name: peers[idx], address: {url: config.peers[peers[idx]].url, host: segURL[0], port: segURL[1]} });
                //console.log('getPeerInfo #', idx, ': ', nwCfg.peers[idx]);
	};
};


function getOrdererInfo() {
	for(var idx in orderers) {
		var segURL = splitURL(config.orderers[orderers[idx]].url);
		//console.log(segURL[0], segURL[1]);
		nwCfg.orderers.push({ name: orderers[idx], address: {url: config.orderers[orderers[idx]].url, host: segURL[0], port: segURL[1]} });
		//console.log('getOrdererInfo #', idx, ': ', nwCfg.orderers[idx]);
	};
};


async function grpcConnectCheck(target, shared_value, object_key) {

	console.log('\n========== grpcConnectCheck fnc : ', target, '\nshared value : ', shared_value, '\n');

  	for(var idx = 0; idx < nwCfg[target].length; idx++){
		console.log(' idx #',idx);
  		logger.debug(nwCfg[target][idx].name);

		try {
	    		var result = await grpcConnect.connect(nwCfg[target][idx].address.host, nwCfg[target][idx].address.port);
			logger.debug(result);

			// console.log('name : ', name);
			// console.log('shared_value : ', shared_value);

			if(nwCfg[target][idx].name != shared_value) {
				shared_value = nwCfg[target][idx].name;
	      			shared_object[object_key] = shared_value;
				logger.debug('Chaged the shared value!');

	      		// const read_only_shared_object = new Shared.Open('./app/peer-ordererfg
			// logger.debug('shared value : ' + read_only_shared_object[object_key]);
	      		// read_only_shared_object.close();
			}

  	 		break;

		} catch (error) {
			logger.debug(error);
		}
		// console.log('=== ping end');

	};

	return shared_value;

};
