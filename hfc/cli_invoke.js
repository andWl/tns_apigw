const util = require('util');
const exec = util.promisify(require('child_process').exec);

async function invoke(cmd) {
  const { stdout, stderr } = await exec(cmd);
  //console.log('stdout:', stdout);
  //console.log('stderr:', stderr);
}

for(var i=0;i<200;i++) {
  var peerNum;

  if (i%3 === 0) {
    peerNum = 0;
  } else if(i%3 === 1) {
    peerNum = 1;
  } else if(i%3 === 2) {
    peerNum = 2;
  }

  var cmd = `docker exec -e \"CORE_PEER_ADDRESS=peer${peerNum}.org1.obchain.net:7051\" fabric_cli_1 peer chaincode invoke -o orderer0.obchain.net:7050 -C obchannel01 -n op_cc -v 1.0 -c \'{\"Args\":[\"setMessage\", \"key${i}\", \"value${i}\"]}\'`;

  //console.log(cmd);

  invoke(cmd);

}
