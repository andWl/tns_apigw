module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : 'API-GW',
      script    : './bin/www',
      instances : '20',
      exec_mode : 'cluster',
      node_args : '',
      env: {
	NODE_ENV: "production",
      },
      env_production : {
        NODE_ENV: 'production'
      },
      log_date_format : "YYYYMMDD HH:mm:ss Z",
      out_file     : "/var/log/ems_log/ems4ob2.log",
	merge_logs      : true,
     }

  ]
};
