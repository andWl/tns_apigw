package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type OpChaincode struct {
}

func main() {
	err := shim.Start(new(OpChaincode))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}

}

// Init initializes chaincode
// ===========================
func (t *OpChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

// Invoke - Our entry point for Invocations
// ========================================
func (t *OpChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("invoke is running " + function)

	// Handle different functions
	if function == "setMessage" {			//save
		return t.setMessage(stub, args)
	} else if function == "getMessage" {	//guery
		return t.getMessage(stub, args)
	}

	fmt.Println("invoke did not find func: " + function) //error
	return shim.Error("Received unknown function invocation")

}

func (t *OpChaincode) setMessage(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2: [0] key, [1] value")
	}

	fmt.Println("-- start setMessage =>")
	fmt.Println(" key: " + args[0]+ "\n value: " + args[1])

	// === Save data set to state DB ===
	err := stub.PutState(args[0], []byte(args[1]))
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("-- end setMessage")
	return shim.Success(nil)

}

func (t *OpChaincode) getMessage(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1: [0] key")
	}

	fmt.Println("-- start getMessage")

	queryResults, err := stub.GetState(args[0])
	if err != nil {
		return shim.Error(err.Error())
	}
	if queryResult == nil {
		return shim.Success(nil)
	}

	fmt.Println(string(queryResults))

	fmt.Println("-- end getMessage")
	return shim.Success(queryResults)

}
