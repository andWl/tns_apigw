'use strict';
var express = require('express');
var router = express.Router();

var invoke = require('../app/invoke-transaction.js');
var query = require('../app/query.js');

var utils = require('../app/utils.js');
var helper = require('../app/helper.js');
var logger = helper.getLogger('gateway');
logger.setLevel('ERROR');

router.post('/chaincode/:chaincodeName', async function(req, res) {
  logger.debug(`End point : POST /chaincodes/${req.params.chaincodeName}`);
  var chaincodeName = req.params.chaincodeName;
  var fcn = req.body.fcn;
  var args = req.body.args;

  req.checkBody('fcn', 'Fcn(Function Name) is required').notEmpty();
  req.checkBody('args', 'args(Arguments) is required').notEmpty();

  var errors = req.validationErrors();
  
  if(errors) {
    res.status(400).send(errors[0]);
    return;
  }

  try {
    var location = utils.getPeers(args);
	//logger.error(location);

    let message = await invoke.invokeChaincode(location, chaincodeName, fcn, args, req.username, req.orgname);
    if(message.success) {
      logger.debug('*** Success Invoke Transactoin');
      res.send('success');
      invoke.sendTransaction(location, message.message);
    } else {
      res.sendStatus(500);
    }

  } catch(error) {
    console.log(error);
    res.status(500).send(error);
  }
});


router.post('/query/chaincode/:chaincodeName', async function(req, res) {
  logger.debug(`End point : GET /chaincodes/${req.params.chaincodeName}`);
  
  var chaincodeName = req.params.chaincodeName;
  var fcn = req.body.fcn;
  var args = req.body.args;

  req.checkParams('chaincodeName', 'ChaincodeName is required').notEmpty();
  req.checkBody('fcn', 'Fcn(Function Name) is required').notEmpty();
  req.checkBody('args', 'args(Arguments) is required').notEmpty();

  var errors = req.validationErrors();
  
  if(errors) {
    res.status(400).send(errors[0]);
    return;
  }

 // args = args.replace(/'/g, '"');
  //args = JSON.parse(args);

  try {
    var location = utils.getPeers(args);
	
    let message = await query.queryChaincode(location, chaincodeName, args, fcn, req.username, req.orgname);
    if(message.success) {
      logger.debug('*** Success Query Transactoin');
      if(message.message == null || message.message == undefined || message.message == '') {
        res.status(400).send(location);
        return;
      } 
      res.send(message.message);
      return;
    } else {
      res.sendStatus(500);
    }
  } catch(err) {
    res.status(500).send('Location : '+ location.peer +  "\nError: " + err);
    return;
  }
});

module.exports = router;
