'use strict';
var express = require('express');
var router = express.Router();

var invoke = require('../app/invoke-transaction.js');
var query = require('../app/query.js');

var utils = require('../app/utils.js');
var helper = require('../app/helper.js');
var logger = helper.getLogger('gateway');
var dateFormat = require('dateformat');


logger.setLevel('INFO');

router.post('/chaincode/:chaincodeName', async function(req, res) {
  logger.debug(`End point : POST /chaincodes/${req.params.chaincodeName}`);
  var chaincodeName = req.params.chaincodeName;
  var fcn = req.body.fcn;
  var args = req.body.args;
  var day=dateFormat(new Date(), "yyyymmdd HH:MM:ss.l");
  var req_ip = req.connection.remoteAddress
  var TxID = global.client.newTransactionID().getTransactionID()  
  
  req.checkBody('fcn', 'Fcn(Function Name) is required').notEmpty();
  req.checkBody('args', 'args(Arguments) is required').notEmpty();

  var errors = req.validationErrors();

  if(errors) {
    res.status(400).send(errors[0]);
    return;
  }

  try {
    var location = utils.getPeers(args);
   // var today = new Date();
	//logger.debug(`${today.format('yymmdd')}`);
     	//logger.debug(`${today}`)
	//console.log(location.channelName);

	//logger.error(location);
    let message = await invoke.invokeChaincode(location, chaincodeName, fcn, args, req.username, req.orgname);
    if(message.success) {
      logger.info('*** Success Invoke Transaction');

	  //setTimeout(function() {

		//res.send(message.message);
		//}, 250);
     // logger.info('[Transaction ID] : '+ message.message);
      res.status(200).send(message.message);

 //     logger.debug(`Time:${now.format("HH:mm:ss")},TxID:${TxID} ,OPType:[1][2],RS:[S][F],EC:[(0-9)]{3,4}],EM:invoke ${message.message}`);
	// invoke success log
	// Time: 발생시간
	// OPType : = 1(도메인등록), 2(도메인조회)
	// SType : 1(BNS), 2(App GW), 3(api-gw), 4(Hyperledger Fabric)
	// Ctype : 컴포넌트 종류-> 0:null / 노드종류별로 정의 (Hyperledger Fabric 경우 Core, Kafka, Zookeeper 등 구분)
	// RS : 성공실패 -> 성공: S / 실패 : F
	// EC : 에러코드 
	// EM : 에러메시지
	
		
	logger.info(`Time:${day},TxID:${TxID},OPType:1,SType:3,CType:0,RS:S,Node:${req_ip}`);	
	//logger.debug(`Time: ,TxID:${TxID},OPType:1,SType:3,CType:0,RS:S,EC:[    (0-9)]{3,4}],EM:invoke ${message.message}`);
		
    } else {
      res.sendStatus(500);
     
   logger.info(`Time:${day},TxID:${TxID},OPType:1,SType:3,CType:0,RS:F,EC:401,EM:"${message.message}",Node:${req_ip}`);
    }
  } catch(error) {
    res.sendStatus(500);
  };
});


router.post('/query/chaincode/:chaincodeName', async function(req, res) {
  logger.debug(`End point : GET /chaincodes/${req.params.chaincodeName}`);

  var chaincodeName = req.params.chaincodeName;
  var fcn = req.body.fcn;
  var args = req.body.args;
  var day=dateFormat(new Date(), "yyyymmdd HH:MM:ss.l");
  var req_ip = req.connection.remoteAddress
  


  req.checkParams('chaincodeName', 'ChaincodeName is required').notEmpty();
  req.checkBody('fcn', 'Fcn(Function Name) is required').notEmpty();
  req.checkBody('args', 'args(Arguments) is required').notEmpty();

  var errors = req.validationErrors();

  if(errors) {
    res.status(400).send(errors[0]);
    return;
  }

 // args = args.replace(/'/g, '"');
  //args = JSON.parse(args);

  try {
    var location = utils.getPeers(args);

    let message = await query.queryChaincode(location, chaincodeName, args, fcn, req.username, req.orgname);
    if(message.success) {
      logger.info('*** Success Query Transactoin');
      logger.info(`Time:${day},TxID:${args},OPType:2,SType:3,CType:0,RS:S,Node:${req_ip}`);

        if(message.message == null || message.message == undefined || message.message == '') {
        res.status(400).send(location);
        return;
      }
      res.send(message.message);
      return;
    } else {
      logger.info(`Time:${day},TxID:${args},OPType:2,SType:3,CType:0,RS:F,EC:402,EM:"${message.message}",Node:${req_ip}`);

      res.sendStatus(500);
    }
  } catch(err) {
    res.status(500).send('Location : 222, '+ location.peer +  "\nError: " + err);
    return;
  }
});

router.post('/test', async function(req, res) {
  setTimeout(function () {  
    res.status(200).send('success');
  }, 50);
  //res.status(200).send('success');
});


module.exports = router;
