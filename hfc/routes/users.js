'use strict';
var log4js = require ('log4js');
var logger = log4js.getLogger('users');
logger.setLevel('DEBUG');

var express = require('express');
var jwt = require('jsonwebtoken');
var expressJWT = require('express-jwt');

require('../config/config.js');
var hfc = require('fabric-client');


var helper = require('../app/helper.js');
var utils = require('../app/utils.js');

var router = express.Router();

var app = express();
app.set('secret', 'thisismysecret');
app.use(expressJWT({
	secret: 'thisismysecret'
}).unless({
	path: ['/api/users']
}));


// Register and enroll user
router.post('/', async function(req, res) {
	var username = req.body.username;
	var orgName = req.body.orgName;
	logger.debug('End point : /users');
	logger.debug('User name : ' + username);
	logger.debug('Org name  : ' + orgName);
	if (!username) {
		res.json(utils.getErrorMessage('\'username\''));
		return;
	}
	if (!orgName) {
		res.json(utils.getErrorMessage('\'orgName\''));
		return;
	}

  console.log(app.get('secret'));

	var token = jwt.sign({
		exp: Math.floor(Date.now() / 1000) + parseInt(hfc.getConfigSetting('jwt_expiretime')),
		username: username,
		orgName: orgName
	}, app.get('secret'));

	let response = await helper.getRegisteredUser(username, orgName, true);
	logger.debug('-- returned from registering the username %s for organization %s',username,orgName);

	if (response && typeof response !== 'string') {
		logger.debug('Successfully registered the username %s for organization %s',username,orgName);
		response.token = token;
		res.json(response);
	} else {
		logger.debug('Failed to register the username %s for organization %s with::%s',username,orgName,response);
		res.json({success: false, message: response});
	}

});

module.exports = router;
