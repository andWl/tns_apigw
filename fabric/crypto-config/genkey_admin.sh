if [ "$#" -ne 2 ]; then
    echo "Usage : ./genkey_admin.sh <AdminName> <DomainName>" >&2
    echo "Example : ./genkey_admin.sh Admin bs" >&2
    exit 1
fi

KEYDIR="./hfc-key-store"
PEER_ORG_ID="$2.kt.com"
USER_ID="$1@${PEER_ORG_ID}"
CRYPTO_ASSETS_DIR="./crypto-config"
SIGNCERT_FILE="${CRYPTO_ASSETS_DIR}/peerOrganizations/${PEER_ORG_ID}/users/${USER_ID}/msp/signcerts/${USER_ID}-cert.pem"
PRIVKEY_STORE="${CRYPTO_ASSETS_DIR}/peerOrganizations/${PEER_ORG_ID}/users/${USER_ID}/msp/keystore"

if [ ! -d "$PRIVKEY_STORE" ]; then
    echo "$PRIVKEY_STORE does not exist. Run crypto gen first!!!" >&2
    exit 1
fi

PRIVKEY_FILENAME=$(ls ${PRIVKEY_STORE})
CREDENTIALS_CERT_ID=${PRIVKEY_FILENAME:0:64}

PRIVKEY_FILE="${CRYPTO_ASSETS_DIR}/peerOrganizations/${PEER_ORG_ID}/users/${USER_ID}/msp/keystore/${CREDENTIALS_CERT_ID}_sk"

mkdir -p "${KEYDIR}"
# extract pubkey from signing cert
openssl x509 -pubkey -noout -in "$SIGNCERT_FILE" > \
  "${KEYDIR}/${CREDENTIALS_CERT_ID}-pub"

# copy private key across
cp "$PRIVKEY_FILE" \
  "${KEYDIR}/${CREDENTIALS_CERT_ID}-priv"

# generate connection profile credential file
SIGNCERT_CONTENTS=$(cat "$SIGNCERT_FILE" | sed -z 's/\n/\\n/g')
cat << EOF > ${KEYDIR}/$11
{
  "name":"$11",
  "mspid":"Org1MSP",
  "roles":null,
  "affiliation":"",
  "enrollmentSecret":"",
  "enrollment":{
    "signingIdentity":"${CREDENTIALS_CERT_ID}",
    "identity":{
      "id":"testIdentity",
      "certificate":"${SIGNCERT_CONTENTS}"
    }
  }
}
EOF
